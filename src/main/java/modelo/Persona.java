
package modelo;

import excepciones.PersonaException;

/**
 *
 * @author Maria del Mar
 */
public class Persona {
    private String nombre;
    private int edad;

    public Persona(String nombre, int edad) throws PersonaException {
        this.nombre = nombre;
        setEdad(edad);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) throws PersonaException {
         if (edad < 0 || edad > 130) {
            throw new PersonaException("Edad incorrecta");
        }
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", edad=" + edad + '}';
    }
}
