package vista;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Maria del Mar
 */
public class EjemploScanner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe una palabra");
        String palabra = sc.nextLine();
        System.out.println("Tu palabra es: " + palabra);
        boolean error = false;
        do {
            sc.nextLine();
            error = false;
            try {
                System.out.println("Escribe un número:");
                int num = sc.nextInt();
                System.out.println("Tu número: " + num);
            } catch (InputMismatchException ex) {
                error = true;
                System.out.println("Te he dicho que pongas un número! No escribas con el codo");
            }
        } while (error);

    }

}
