package vista;

import excepciones.PersonaException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import modelo.Persona;

/**
 *
 * @author Maria del Mar
 */
public class Inicio {

    // NUNCA JAMÁS DE LA VIDA ESTE MÉTODO VA A TENER UN THROWS!!!
    public static void main(String[] args) {
        int numero = askInt("Edad:");
        try {
            Persona p = new Persona("Mar", numero);
            System.out.println("Persona registrada.");
            System.out.println("Datos de la persona: " + p);
            int nuevaEdad = askInt("Indica una nueva edad: ");
            p.setEdad(nuevaEdad);
            System.out.println("Nuevos datos: " + p);
        } catch (PersonaException ex) {
            System.out.println(ex.getMessage());
        }

    }

    // Devuelve un número entero introducido por el usuario 
    public static int askInt(String message) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = 0;
        boolean error = false;
        do {
            System.out.println(message);
            try {
                error = false;
                num = Integer.parseInt(br.readLine());
            } catch (IOException ex) {
                error = true;
                System.out.println("Este mensaje no se verá nunca (en principio)");
                System.out.println(ex.getMessage());
            } catch (NumberFormatException ex) {
                error = true;
                System.out.println("Te he dicho que pongas un número!!!");
            }
        } while (error);
        return num;
    }
}
