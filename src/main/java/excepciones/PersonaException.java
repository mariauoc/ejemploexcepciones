
package excepciones;

/**
 *
 * @author Maria del Mar
 */
public class PersonaException extends Exception {

    public PersonaException(String message) {
        super(message);
    }
}
